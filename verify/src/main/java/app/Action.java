package app;

import java.util.Arrays;

public class Action {

    private final String action;
    private final String[] validActions = new String[] {
            "transfer", "withdraw"
    };

    public Action(String vin) throws RuntimeException {
        validate(vin);
        this.action = vin;
    }

    public String getAction() {
        return this.action;
    }

    private void validate(String input) throws RuntimeException {

        if (input == null) {
            throw new RuntimeException("action is null");
        }

        if (input.trim().length() == 0) {
            throw new RuntimeException("action is empty");
        }

        if (!Arrays.asList(validActions).contains(input)) {
            throw new RuntimeException("action is invalid");
        }

    }

}
