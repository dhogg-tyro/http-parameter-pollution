package app;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.ResponseEntity;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Value;

@RestController
public class VerifyController {

    @Value("${paymenturl}")
    private String paymentUrl;

    @PostMapping("/")
    public String res(HttpServletRequest request) {
        System.out.println("XXXXXXXXXXXXXXXXXXX action param: " + request.getParameter("action"));
        System.out.println("XXXXXXXXXXXXXXXXXXX amount param: " + request.getParameter("amount"));

        Amount amount = new Amount(request.getParameter("amount"));
        Action action = new Action(request.getParameter("action"));

        if (action.getAction().equals("transfer")) {
            System.out.println("Verify Controller: Going to transfer $"+amount.getAmount());
            RestTemplate restTemplate = new RestTemplate();
            String fakePaymentUrl = this.paymentUrl;  //Internal fake payment micro-service

            // TODO: use url encoding:
            String fullUrl = fakePaymentUrl + "?action=" + action.getAction() + "&amount=" + amount.getAmount();
            ResponseEntity<String> response
                = restTemplate.getForEntity(
                    fullUrl,
                    String.class);
            return response.getBody();
        } else if (action.getAction().equals("withdraw")) {
            return "Verify Controller: Sorry, you can only make transfer";
        } else {
            return "Verify Controller: You must specify action: transfer or withdraw";
        }
    }

}
