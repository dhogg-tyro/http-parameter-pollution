package app;

public class Amount {

    private final Integer amount;

    public Amount(String amount) throws RuntimeException {
        Integer parsedAmount = parse(amount);
        this.amount = parsedAmount;
    }

    public Integer getAmount() {
        return this.amount;
    }

    private Integer parse(String amount) throws RuntimeException {
        Integer parsedAmount = Integer.parseInt(amount);
        if (parsedAmount < 0) {
            throw new RuntimeException("amount is less than 0");
        }
        return parsedAmount;
    }

}
