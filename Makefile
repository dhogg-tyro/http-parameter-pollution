APP=insecure-programming-app

all: build

test:
	docker-compose -f docker-compose.test.yml up -d payment
	docker-compose -f docker-compose.test.yml build --build-arg GRADLETASK=test verify

securitytest:
	docker-compose -f docker-compose.test.yml up -d payment
	docker-compose -f docker-compose.test.yml build --build-arg GRADLETASK=securitytest verify

build:
	docker-compose build

run:
	docker-compose up -d

stop:
	docker-compose down

logs:
	docker-compose logs -f

run-tests:
	docker-compose -f docker-compose.test.yml up

down-tests:
	docker-compose -f docker-compose.test.yml down

clean:
	docker-compose down
	docker-compose rm -f
	docker-compose -f docker-compose.test.yml down
	docker-compose -f docker-compose.test.yml rm -f
	docker system prune

curl-test-ok:
	# OK: Fake Payment Controller: Successfully transfered $520
	curl -X POST http://localhost:8080 -d 'action=transfer&amount=520'

curl-test-hack:
	# BAD: Fake Payment Controller: Successfully withdrew $100
	curl -X POST http://localhost:8080 -d 'action=transfer&amount=100%26action=withdraw'

.PHONY: all test securitytest clean run-tests curl-test-ok curl-test-hack
